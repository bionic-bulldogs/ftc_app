package org.firstinspires.ftc.teamcode.Walbots7175.internal;

/**
 * Created by nicolas on 1/28/18 in ftc_app.
 * <p>
 * Copyright (c) ©2018 Nicolas Hohaus
 * Copyright (c) ©2018 Walbots (7175)
 * <p>
 * Resource: https://gitlab.com/roboticsclub/ftc_app
 * Contact: nico@walbots.com, team@walbots.com
 */


import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.HardwareController;


/**
 * The class PowerAdjustController is designed to adjust the power value that is given to motors.
 * This adjustment is based one some gamepad keys and some predefined factors that make the output
 * more convenient and natural.
 *
 * - Left_Trigger:  Adjusting the power to be always slower than the original enabling the robot
 *                  to be controlled very precisely
 *
 * - Right_Trigger: Adding up to 0.5 to the power value enabling to reach a maximum power of 1
 *                  if the robot needs to drive faster or motors need more power to complete a
 *                  specific task.
 */
public class PowerAdjustController
{
    private Gamepad gamepad;    //The gamepad where the keys for the adjustments can be pressed
    private float power = 0;    //The power value to adjust

    /**
     * Instantiates a PowerAdjustController with the gamepad, on which the adjustment keys can be
     * pressed to control specific adjustment factors.
     *
     * @param gamepad The gamepad with keys that are considered for the adjustment factors
     */
    public PowerAdjustController(Gamepad gamepad)
    {
        this.gamepad = gamepad;
    }

    /**
     * The getAdjustedPower methods takes a given power and adjusts it's value by some predefined
     * factors but also considering the gamepad input from some special keys.
     *
     * - Left_Trigger:  Adjusting the power to be always slower than the original enabling the robot
     *                  to be controlled very precisely
     *
     * - Right_Trigger: Adding up to 0.5 to the power value enabling to reach a maximum power of 1
     *                  if the robot needs to drive faster or motors need more power to complete a
     *                  specific task.
     *
     * @param powerIn The original power value
     * @return The adjusted more convenient and natural power value
     */
    public float getAdjustedPower(float powerIn)
    {
        //Set variables
        boolean isPowerNegative = powerIn < 0;  //Save the sign
        this.power = Math.abs(powerIn);         //We only adjust positive power

        //Adjust power
        adjustPower();
        adjustPowerByGamepadKey();

        //Restore the sign of power
        if (isPowerNegative)
        {
            power = -power;
        }

        //Return adjusted power
        return power;
    }

    /**
     * The adjustPower() method makes sure that we are using only half of the possible power by
     * default so that the robot is moving in a controllable pace. (The full power can still be
     * reached by pressing the right trigger all the way down)
     */
    private void adjustPower()
    {
        power /= 2;
    }

    /**
     * The adjustPowerByGamepadKey() method adjusts the power value dependent on the input of the
     * right and left trigger. The left trigger has a quadratic function making the robot slower and
     * the right trigger has a linear function making the robot faster. This enables the drivers to
     * control the robot very precisely and reliably.
     */
    private void adjustPowerByGamepadKey()
    {
        float leftTriggerPowerOffset = (float) (0.15 + (gamepad.left_trigger * gamepad.left_trigger) / 3);
        float rightTriggerPowerOffset = gamepad.right_trigger / 2;

        power -= leftTriggerPowerOffset; //make it slower
        power += rightTriggerPowerOffset; //make it faster

        power = Range.clip(power, HardwareController.MIN_MOTOR_POWER, power);
    }
}

